import React from 'react';
import './Alert.css';
const Alert = (props) => {
    return (
       props.warning ? <div className={['Alert', props.type].join(' ')}>
            {props.dismiss ? <button onClick={props.dismiss}>X</button> : null}
            {props.children}
        </div> : null
    )
};

export default Alert;