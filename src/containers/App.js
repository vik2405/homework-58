import React, {Component} from 'react';
import './App.css';
import Modal from "../components/UI/Modal/Modal";
import Alert from "../components/UI/Alert/Alert";

class App extends Component {


    purchaseHandler = () => {
        this.setState({purchasing: true});
    };
    purchaseCancelHandler = () => {
        this.setState({purchasing: false});
    };

    alertCloseHandler = (type) => {
        let alerts = [...this.state.alerts];
        let index = alerts.findIndex(alert => alert.type === type);
        let alert = alerts[index];
        alert.visible = false;
        alerts[index] = alert;
        this.setState({warning: false})
    };
    state = {
        purchasing: false,
        alerts: [
            {type: 'success', visible: true, dismiss: null},
            {type: 'warning', visible: true, dismiss: this.alertCloseHandler},
            {type: 'primary', visible: true, dismiss: null},
            {type: 'danger', visible: true, dismiss: this.alertCloseHandler},
        ]
    };
    render() {
        return (
            <div className="App">
                <button className="open" onClick={this.purchaseHandler}>Open modal windows</button>
                <Modal
                    show={this.state.purchasing}
                    title="Modal Title"
                    closed={this.purchaseCancelHandler}>
                    <p className="modal">Some information</p>
                </Modal>

                {this.state.alerts.map((alert, i) => {
                        return (
                            <Alert
                                key={i}
                                type={alert.type}
                                dismiss={alert.dismiss ? () => alert.dismiss(alert.type) : null}
                                warning={alert.visible}>
                                <p><b>Alert :</b> {alert.type}</p>
                            </Alert>
                        )
                    })}


            </div>
        );
    }
}

export default App;
